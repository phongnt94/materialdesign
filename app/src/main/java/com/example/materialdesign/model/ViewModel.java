package com.example.materialdesign.model;



/**
 * Created by phong on 7/15/2015.
 */
public class ViewModel {

    private String title;
    private String link;
    private String image;
    private String pubDate;

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public ViewModel() {

    }

    public ViewModel(String title, String link, String image) {
        this.title = title;
        this.link = link;
        this.image = image;
    }

    public ViewModel(String title, String link, String image, String pubDate) {
        this.title = title;
        this.link = link;
        this.image = image;
        this.pubDate = pubDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

