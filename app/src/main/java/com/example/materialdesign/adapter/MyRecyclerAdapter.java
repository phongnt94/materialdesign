package com.example.materialdesign.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.materialdesign.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.example.materialdesign.model.ViewModel;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    private List<ViewModel> items;
    private Context ct;

    public MyRecyclerAdapter(List<ViewModel> items, Context context) {
        this.items = items;
        this.ct = context;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewmodel_item, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        ViewModel item = items.get(position);
        holder.title.setText(item.getTitle());
        //holder.photo.setImageBitmap(null);
        //Picasso.with(holder.photo.getContext()).cancelRequest(holder.photo);

        if( item.getImage()!=null && (!item.getImage().equals("")) ) {
            Picasso.with(holder.photo.getContext()).load(item.getImage()).into(holder.photo);
        }else {
            holder.photo.setImageResource(R.drawable.abc_btn_check_to_on_mtrl_000);
        }

        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }
    public ViewModel getItem(int position) {
        return items.get(position);
    }

    public void add(ViewModel item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(ViewModel item) {
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView photo;
        public TextView title;
        public TextView link;


        public ViewHolder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.photo);
            title = (TextView) itemView.findViewById(R.id.title);
            link = (TextView)itemView.findViewById(R.id.link);
        }
    }
}
