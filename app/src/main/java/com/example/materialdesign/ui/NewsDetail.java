package com.example.materialdesign.ui;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.example.materialdesign.R;
import com.example.materialdesign.parses.ParseNew;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.AsyncHttpResponse;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;


public class NewsDetail extends ActionBarActivity {
    private String link;
    private String title;
    private String image;
    TextView textViewTitle;
    ImageView imageViewContent;
    TextView textViewContent;
    String content;
    WebView webview;
    Toolbar toolbar;
    String html;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        textViewTitle = (TextView)findViewById(R.id.textViewTitle);
        textViewContent = (TextView)findViewById(R.id.textViewContent);
        imageViewContent =(ImageView)findViewById(R.id.imageViewContent);
        final ProgressBar progressBar =(ProgressBar)findViewById(R.id.progessBar);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        webview = (WebView)this.findViewById(R.id.webView);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);


        Bundle bund = getIntent().getExtras();
        if(bund!=null)
        {
            link = bund.getString("link");
            title = bund.getString("title");
            image = bund.getString("image");
            toolbar.setTitle(title);
        }
        AsyncHttpClient.getDefaultInstance().executeString(new AsyncHttpGet(link),new AsyncHttpClient.StringCallback(){

            @Override
            public void onCompleted(Exception e, AsyncHttpResponse source, String result) {

                if(e!=null){
                    final String error = e.toString();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            textViewContent.setText("Erorr!");
                            Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        }
                    });

                    return;
                }

                org.jsoup.nodes.Document doc =  Jsoup.parse(result);
                 content = new ParseNew().getText(doc);
                html = new ParseNew().getString(doc);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewTitle.setText(title);
                       /* textViewContent.setText(content);
                        Picasso.with(getApplicationContext()).load(image).into(imageViewContent);*/
                        webview.loadDataWithBaseURL("",html , "text/html", "UTF-8", "");
                        progressBar.setVisibility(View.GONE);

                    }
                });





            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_news_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
