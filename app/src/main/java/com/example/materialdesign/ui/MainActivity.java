package com.example.materialdesign.ui;

import android.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.materialdesign.R;
import com.example.materialdesign.config.Varriable;


public class MainActivity extends ActionBarActivity {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    int id;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    ContentFragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        fragment = ContentFragment.newInstance(0);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
        setTitle("Trang chủ");

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);
                //Closing drawer on item click
                drawerLayout.closeDrawers();
                //Check to see which item was being clicked and perform appropriate action
               /* ContentFragment fragment = null;
                FragmentManager fragmentManager = getFragmentManager();
*/
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.trangchu:
                        Toast.makeText(getApplicationContext(), "home", Toast.LENGTH_SHORT).show();
                        id = 0;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;
                    case R.id.vandenong:
                        id = 1;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        Toast.makeText(getApplicationContext(), "van de nong", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.gocnhin:
                        id = 2;
                        Toast.makeText(getApplicationContext(), "goc nhin", Toast.LENGTH_SHORT).show();
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;

                    case R.id.trithuc:
                        id = 3;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;

                    case R.id.lichsu:
                        id = 4;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;

                    case R.id.nghethuat:
                        id = 5;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;


                    case R.id.moitruong:
                        id = 6;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;


                    case R.id.khoanhkhac:

                        id = 7;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;


                    case R.id.camxuc:
                        id = 8;
                        changeContent(id);
                        setTitle(Varriable.category[id]);
                        // For rest of the options we just show a toast on click
                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                }
                Log.e("id", String.valueOf(id));



                return true;


            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();


    }
    public void changeContent(int id){
        fragment = ContentFragment.newInstance(id);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
