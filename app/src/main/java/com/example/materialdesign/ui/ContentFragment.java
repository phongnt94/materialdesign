package com.example.materialdesign.ui;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.materialdesign.R;

import java.util.ArrayList;

import com.example.materialdesign.adapter.MyRecyclerAdapter;
import com.example.materialdesign.adapter.RecyclerItemClickListener;
import com.example.materialdesign.config.Varriable;
import com.example.materialdesign.model.ViewModel;
import com.example.materialdesign.parses.RSSFeed;
import com.example.materialdesign.parses.RSSParser;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.AsyncHttpResponse;


public class ContentFragment extends Fragment {


    int id;
    RSSFeed rssFeed;
    // List<ViewModel> rssItems = new ArrayList<>();
    String rss_url = "http://reds.vn/index.php/thoi-su?format=feed&type=rss";

    ArrayList<ViewModel> listAdapter = new ArrayList<>();
    private ProgressBar progessBar;
    private RecyclerView mRecyclerView;
    private MyRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ProgressDialog pDialog;


    public static ContentFragment newInstance(int id) {
        ContentFragment page = new ContentFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        page.setArguments(args);
        return page;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = getArguments().getInt("id", 0);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.content_fragment, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_news);
        progessBar = (ProgressBar) rootView.findViewById(R.id.progessBar);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        Log.e("link",Varriable.LINKS[id]);

        AsyncHttpClient.getDefaultInstance().executeString(new AsyncHttpGet(Varriable.LINKS[id]),
                new AsyncHttpClient.StringCallback() {
                    // Callback is invoked with any exceptions/errors, and the result, if available.
                    @Override
                    public void onCompleted(Exception e, AsyncHttpResponse response, String result) {
                        if (e != null) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        RSSParser parser = new RSSParser();
                        listAdapter = parser.getRSSFeedItems(result);
                        mAdapter = new MyRecyclerAdapter(listAdapter, getActivity());
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.setAdapter(mAdapter);
                                progessBar.setVisibility(View.GONE);


                            }
                        });


                    }
                });
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        ViewModel rowItem = mAdapter.getItem(position);
                        Log.e("click", rowItem.getLink());
                        Intent i = new Intent(getActivity(), NewsDetail.class);
                        i.putExtra("title", rowItem.getTitle());
                        i.putExtra("link", rowItem.getLink());
                        i.putExtra("image", rowItem.getImage());
                        startActivity(i);
                        //getActivity().finish();
                    }
                })
        );


        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }


}

