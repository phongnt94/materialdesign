package com.example.materialdesign.parses;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by chien.phi on 8/21/2015.
 */
public class ParseNew {
    Document doc;
    private String text=" ";
    private static ParseNew instance = null;

    public String getText(Document document){
            Elements p = document.getElementsByTag("p");
            for (Element element  : p) {
                text = text + element.text()+"\n";
            }
            return text;
    }
    public String getString(Document document){
        Elements p = document.getElementsByTag("p");
        for (Element element  : p) {
            text = text + element.toString()+"\n";
        }
        return text;


    }

}
